package docs

import ucontroller "gitlab.com/lear4212225/goboilerplate/internal/modules/user/controller"

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/user/profile/{userID} user profileRequest
// Получение информации о текущем пользователе.
// security:
//		- Bearer:[]
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body ucontroller.ProfileResponse
}
